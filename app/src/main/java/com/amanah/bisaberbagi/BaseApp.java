package com.amanah.bisaberbagi;

import android.app.Application;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;

import okhttp3.OkHttpClient;

public class BaseApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addNetworkInterceptor((new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
                    @Override
                    public void log(String message) {
                        Log.d("bisaberbagi",message);
                    }
                }))
                        .setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
        AndroidNetworking.initialize(this);
    }
}
