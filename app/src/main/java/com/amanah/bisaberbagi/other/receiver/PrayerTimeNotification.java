package com.amanah.bisaberbagi.other.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PrayerTimeNotification extends BroadcastReceiver implements PrayerTimeNotificationContract{

    @Override
    public void onReceive(Context context, Intent intent) {

    }

    @Override
    public void setAdzanNotification() {
        
    }

    @Override
    public void setAlarmNotification() {

    }

    @Override
    public void showNotification() {

    }

    @Override
    public boolean isAlarmActive() {
        return false;
    }
}
