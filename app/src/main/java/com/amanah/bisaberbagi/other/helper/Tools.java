package com.amanah.bisaberbagi.other.helper;


import android.util.Log;

import com.amanah.bisaberbagi.model.DateConversion;
import com.amanah.bisaberbagi.model.JadwalSholat;
import com.amanah.bisaberbagi.model.Qibla;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.parassidhu.simpledate.SimpleDateKt;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Tools {
    private ToolsInterface.OnGetMonthlySholatListener onGetMonthlySholatListener;
    private ToolsInterface.OnGetDateListener onGetDateListener;
    private ToolsInterface.OnGetQiblaListener onGetQiblaListener;
    private ToolsInterface.OnGetDailySholatListener onGetDailySholatListener;
    private String lat, lang, date;
    private final String errorReq = "ERROR REQUEST";

    //todo: Tools need SimpleDateKt and AndroidNetworking library, install it before use
    //todo also please setup the POJO Model for API Response (exp : JadwalSholat)

    //getDate methode return current date with optional adjustment (exp: -1 day or +1 day)
    public static String getDate(int adjustment){
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, adjustment);

        //return in format of DD-MM-YY or example : "12-09-2019"
        return SimpleDateKt.toDateStandardInDigits(calendar.getTime());
    }

    public static String getDay(){
        return new SimpleDateFormat("dd", Locale.US).format(Calendar.getInstance().getTime());
    }

    public static String getDayWithAdjust(int adjustment){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, adjustment);
        return new SimpleDateFormat("dd", Locale.ENGLISH).format(calendar.getTime());
    }

    public static String getMonthWithDayAdjustment(int dayAdjustment){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, dayAdjustment);
        return new SimpleDateFormat("MM", Locale.US).format(calendar.getTime());
    }

    public static String getMonth(){
        return new SimpleDateFormat("MM", Locale.US).format(Calendar.getInstance().getTime());
    }

    public static String getYear(){
        return new SimpleDateFormat("yyyy", Locale.US).format(Calendar.getInstance().getTime());
    }

    public static String getHours(){
        return new SimpleDateFormat("HH", Locale.US).format(Calendar.getInstance().getTime());
    }


    public static Tools goTools(){
        return new Tools();
    }

    private void requestMonthlySholat(final String lat, final String lang, final String month, final String year){

        //todo set the parameter for prayer times
        String url_prayer = "http://api.aladhan.com/v1/calendar";
        AndroidNetworking.get(url_prayer)
                .setPriority(Priority.HIGH)
                .addQueryParameter("latitude", lat)
                .addQueryParameter("longitude", lang)
                .addQueryParameter("method", "5")
                .addQueryParameter("month", month)
                .addQueryParameter("year", year)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if(response!=null){
                            Log.d("Berbagi", "onMonthlySholat");
                            onGetMonthlySholatListener.getJsonMonthlySholat(response);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d("Berbagi", "error");


                    }
                });
    }

    private void requestDailySholat(String lat, String lang, String date){
        Log.d("DialogSholat", "loading new");

        String url_jadwalsholat = "https://time.siswadi.com/pray/";
        AndroidNetworking.get(url_jadwalsholat)
                .setPriority(Priority.HIGH)
                .addQueryParameter("lat", lat)
                .addQueryParameter("lng", lang)
                .addQueryParameter("datetime", date)
                .addQueryParameter("method", "5")
                .build()
                .getAsObject(JadwalSholat.class, new ParsedRequestListener<JadwalSholat>() {
                    @Override
                    public void onResponse(JadwalSholat js) {
                        if(js != null){
                            if(js.getData() != null){
                                onGetDailySholatListener.getDailySholat(js);
                            }else {
                                onGetDailySholatListener.onError("errors");

                            }
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        Log.d("networingError", anError.getErrorBody());
                    }
                });
    }

    private void requestDateConversion(String endPointMethod, String date){
        //endpoint method is gToH for Georgian to Hijri and hToG for Hijri to Georgian
        AndroidNetworking.get("http://api.aladhan.com/v1/" + endPointMethod)
                .setPriority(Priority.HIGH)
                .addQueryParameter("date", date) //date format must be dd-mm-yyy
                .build()
                .getAsObject(DateConversion.class, new ParsedRequestListener<DateConversion>() {
                    @Override
                    public void onResponse(DateConversion response) {
                        if(response.getData().getHijri() != null){
                            onGetDateListener.getDate(response);
                        }else {
                            onGetDateListener.onError(errorReq);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        onGetDateListener.onError(anError.getErrorBody());
                    }
                });

    }

    private void requestQiblaDirection(String lat, String lng){
        AndroidNetworking.get("https://time.siswadi.com/qibla")
                .setPriority(Priority.HIGH)
                .addQueryParameter("lat", lat)
                .addQueryParameter("lng", lng)
                .build().getAsObject(Qibla.class, new ParsedRequestListener<Qibla>() {
            @Override
            public void onResponse(Qibla response) {
                if(response.getData().getKabah()!=null){
                    onGetQiblaListener.getQiblaDirection(response);
                }else {
                    onGetQiblaListener.onError(errorReq);
                }
            }

            @Override
            public void onError(ANError anError) {
                onGetQiblaListener.onError(anError.getErrorBody());
            }
        });
    }

    //todo please run setData Method before request the "jadwal sholat" API
    public Tools setData(String lat, String lang, String date){
        this.lat = lat;
        this.lang = lang;
        this.date = date;
        return this;
    }

    public void getMonthlySholat(ToolsInterface.OnGetMonthlySholatListener onGetMonthlySholatListener){
        this.onGetMonthlySholatListener = onGetMonthlySholatListener;
        requestMonthlySholat(lat, lang, Tools.getMonth(), Tools.getYear());
    }

    public void getMonthlySholatBySelf(String lat, String lang, String month, String year, ToolsInterface.OnGetMonthlySholatListener listener){
        this.onGetMonthlySholatListener = listener;
        requestMonthlySholat(lat, lang, month, year);
    }

    public void getDailySholatBySelf(String lat, String lang, String date, ToolsInterface.OnGetDailySholatListener listener){
        this.onGetDailySholatListener = listener;
        requestDailySholat(lat, lang, date);
    }

    public void getDate(String endpoint, String date, ToolsInterface.OnGetDateListener onGetDateListener){
        this.onGetDateListener = onGetDateListener;
        requestDateConversion(endpoint, date);
    }

    public void getQibla(ToolsInterface.OnGetQiblaListener onGetQiblaListener){
        this.onGetQiblaListener = onGetQiblaListener;
        requestQiblaDirection(lat, lang);
    }



    //special interface for api
    public interface ToolsInterface {
        interface OnGetMonthlySholatListener {
            void getJsonMonthlySholat(JSONObject jsonObject);
            void onError(String msg);
        }

        interface OnGetDailySholatListener {
            void getDailySholat(JadwalSholat jadwalSholat);
            void onError(String msg);
        }

        interface OnGetDateListener {
            void getDate(DateConversion dateConversion);
            void onError(String msg);
        }

        interface OnGetQiblaListener {
            void getQiblaDirection(Qibla qibla);
            void onError(String msg);
        }
    }
}
