package com.amanah.bisaberbagi.other.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.amanah.bisaberbagi.R;

public class SharedPrefHelper {

    public static class Adjustment{
        public static String INT_DATE_ADJUSTMENT = "date_adjustment";
        public static String STRING_JSON_MONTHLY_SHOLAT = "json_monthly_sholat";
        public static String FLOAT_LAST_QIBLA_AZIMUTH = "last_qibla_azimuth";
        public static String STRING_LAST_LET_COORDINAT = "last_let_coordinat";
        public static String STRING_LAST_LANG_COORDINAT = "last_lang_coordinat";
        public static String STRING_LAST_LOCATION_NAME = "last_location_name";
        public static String STRING_LAST_LOCATION_NAME_DETAIL = "last_location_name_detail";

    }

    public static class NotifAdjust{
        public static String SUBUH = "subuh_notif";
        public static String DZUHUR = "dzuhur_notif";
        public static String ASHAR = "ashar_notif";
        public static String MAGHRIB = "maghrib_notif";
        public static String ISYA = "isya_notif";
    }




    public static SharedPreferences getSharedPreferencesAdjust(Context context){
        return context.getSharedPreferences(context.getString(R.string.sharedpref_adjust_key), Context.MODE_PRIVATE);
    }

    public static SharedPreferences getSharedPreferencesNotifAdjust(Context context){
        return context.getSharedPreferences(context.getString(R.string.sharedpref_notif_adjust_key), Context.MODE_PRIVATE);
    }

}
