package com.amanah.bisaberbagi.other.receiver;

public interface PrayerTimeNotificationContract {
    void setAdzanNotification();
    void setAlarmNotification();
    void showNotification();
    boolean isAlarmActive();
}
