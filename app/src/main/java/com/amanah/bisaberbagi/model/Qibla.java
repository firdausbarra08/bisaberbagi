package com.amanah.bisaberbagi.model;

import com.google.gson.annotations.SerializedName;

public class Qibla{

	@SerializedName("data")
	private Data data;

	@SerializedName("location")
	private Location location;

	@SerializedName("status")
	private String status;

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setLocation(Location location){
		this.location = location;
	}

	public Location getLocation(){
		return location;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public class Location{

		@SerializedName("address")
		private String address;

		@SerializedName("latitude")
		private String latitude;

		@SerializedName("longitude")
		private String longitude;

		public void setAddress(String address){
			this.address = address;
		}

		public String getAddress(){
			return address;
		}

		public void setLatitude(String latitude){
			this.latitude = latitude;
		}

		public String getLatitude(){
			return latitude;
		}

		public void setLongitude(String longitude){
			this.longitude = longitude;
		}

		public String getLongitude(){
			return longitude;
		}
	}

	public class Data{

		@SerializedName("derajat")
		private double derajat;

		@SerializedName("image")
		private String image;

		@SerializedName("kabah")
		private String kabah;

		@SerializedName("kompas")
		private String kompas;

		public void setDerajat(double derajat){
			this.derajat = derajat;
		}

		public double getDerajat(){
			return derajat;
		}

		public void setImage(String image){
			this.image = image;
		}

		public String getImage(){
			return image;
		}

		public void setKabah(String kabah){
			this.kabah = kabah;
		}

		public String getKabah(){
			return kabah;
		}

		public void setKompas(String kompas){
			this.kompas = kompas;
		}

		public String getKompas(){
			return kompas;
		}
	}
}