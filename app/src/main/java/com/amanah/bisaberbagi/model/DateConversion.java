package com.amanah.bisaberbagi.model;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DateConversion{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private Data data;

	@SerializedName("status")
	private String status;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public class Data{

		@SerializedName("hijri")
		private Hijri hijri;

		@SerializedName("gregorian")
		private Gregorian gregorian;

		public void setHijri(Hijri hijri){
			this.hijri = hijri;
		}

		public Hijri getHijri(){
			return hijri;
		}

		public void setGregorian(Gregorian gregorian){
			this.gregorian = gregorian;
		}

		public Gregorian getGregorian(){
			return gregorian;
		}

		public class Designation{

			@SerializedName("expanded")
			private String expanded;

			@SerializedName("abbreviated")
			private String abbreviated;

			public void setExpanded(String expanded){
				this.expanded = expanded;
			}

			public String getExpanded(){
				return expanded;
			}

			public void setAbbreviated(String abbreviated){
				this.abbreviated = abbreviated;
			}

			public String getAbbreviated(){
				return abbreviated;
			}
		}

		public class Hijri{

			@SerializedName("date")
			private String date;

			@SerializedName("month")
			private Month month;

			@SerializedName("holidays")
			private List<Object> holidays;

			@SerializedName("year")
			private String year;

			@SerializedName("format")
			private String format;

			@SerializedName("weekday")
			private Weekday weekday;

			@SerializedName("designation")
			private Designation designation;

			@SerializedName("day")
			private String day;

			public void setDate(String date){
				this.date = date;
			}

			public String getDate(){
				return date;
			}

			public void setMonth(Month month){
				this.month = month;
			}

			public Month getMonth(){
				return month;
			}

			public void setHolidays(List<Object> holidays){
				this.holidays = holidays;
			}

			public List<Object> getHolidays(){
				return holidays;
			}

			public void setYear(String year){
				this.year = year;
			}

			public String getYear(){
				return year;
			}

			public void setFormat(String format){
				this.format = format;
			}

			public String getFormat(){
				return format;
			}

			public void setWeekday(Weekday weekday){
				this.weekday = weekday;
			}

			public Weekday getWeekday(){
				return weekday;
			}

			public void setDesignation(Designation designation){
				this.designation = designation;
			}

			public Designation getDesignation(){
				return designation;
			}

			public void setDay(String day){
				this.day = day;
			}

			public String getDay(){
				return day;
			}
		}

		public class Gregorian{

			@SerializedName("date")
			private String date;

			@SerializedName("month")
			private Month month;

			@SerializedName("year")
			private String year;

			@SerializedName("format")
			private String format;

			@SerializedName("weekday")
			private Weekday weekday;

			@SerializedName("designation")
			private Designation designation;

			@SerializedName("day")
			private String day;

			public void setDate(String date){
				this.date = date;
			}

			public String getDate(){
				return date;
			}

			public void setMonth(Month month){
				this.month = month;
			}

			public Month getMonth(){
				return month;
			}

			public void setYear(String year){
				this.year = year;
			}

			public String getYear(){
				return year;
			}

			public void setFormat(String format){
				this.format = format;
			}

			public String getFormat(){
				return format;
			}

			public void setWeekday(Weekday weekday){
				this.weekday = weekday;
			}

			public Weekday getWeekday(){
				return weekday;
			}

			public void setDesignation(Designation designation){
				this.designation = designation;
			}

			public Designation getDesignation(){
				return designation;
			}

			public void setDay(String day){
				this.day = day;
			}

			public String getDay(){
				return day;
			}
		}

		public class Month{

			@SerializedName("number")
			private int number;

			@SerializedName("en")
			private String en;

			@SerializedName("ar")
			private String ar;

			public void setNumber(int number){
				this.number = number;
			}

			public int getNumber(){
				return number;
			}

			public void setEn(String en){
				this.en = en;
			}

			public String getEn(){
				return en;
			}

			public void setAr(String ar){
				this.ar = ar;
			}

			public String getAr(){
				return ar;
			}
		}

		public class Weekday{

			@SerializedName("en")
			private String en;

			@SerializedName("ar")
			private String ar;

			public void setEn(String en){
				this.en = en;
			}

			public String getEn(){
				return en;
			}

			public void setAr(String ar){
				this.ar = ar;
			}

			public String getAr(){
				return ar;
			}
		}
	}
}