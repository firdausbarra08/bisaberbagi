package com.amanah.bisaberbagi.model;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JadwalSholat{

	@SerializedName("debug")
	private Debug debug;

	@SerializedName("data")
	private Data data;

	@SerializedName("location")
	private Location location;

	@SerializedName("time")
	private Time time;

	@SerializedName("status")
	private String status;

	public void setDebug(Debug debug){
		this.debug = debug;
	}

	public Debug getDebug(){
		return debug;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setLocation(Location location){
		this.location = location;
	}

	public Location getLocation(){
		return location;
	}

	public void setTime(Time time){
		this.time = time;
	}

	public Time getTime(){
		return time;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public class Location{

		@SerializedName("latitude")
		private String latitude;

		@SerializedName("longitude")
		private String longitude;

		public void setLatitude(String latitude){
			this.latitude = latitude;
		}

		public String getLatitude(){
			return latitude;
		}

		public void setLongitude(String longitude){
			this.longitude = longitude;
		}

		public String getLongitude(){
			return longitude;
		}
	}

	public class Time{

		@SerializedName("date")
		private String date;

		@SerializedName("offset")
		private int offset;

		@SerializedName("timezone")
		private String timezone;

		@SerializedName("time")
		private String time;

		public void setDate(String date){
			this.date = date;
		}

		public String getDate(){
			return date;
		}

		public void setOffset(int offset){
			this.offset = offset;
		}

		public int getOffset(){
			return offset;
		}

		public void setTimezone(String timezone){
			this.timezone = timezone;
		}

		public String getTimezone(){
			return timezone;
		}

		public void setTime(String time){
			this.time = time;
		}

		public String getTime(){
			return time;
		}
	}

	public class Debug{

		@SerializedName("sunrise")
		private String sunrise;

		@SerializedName("sunset")
		private String sunset;

		public void setSunrise(String sunrise){
			this.sunrise = sunrise;
		}

		public String getSunrise(){
			return sunrise;
		}

		public void setSunset(String sunset){
			this.sunset = sunset;
		}

		public String getSunset(){
			return sunset;
		}
	}

	public class Data{

		@SerializedName("Sunset")
		private String sunset;

		@SerializedName("Asr")
		private String asr;

		@SerializedName("Isha")
		private String isha;

		@SerializedName("Fajr")
		private String fajr;

		@SerializedName("Dhuhr")
		private String dhuhr;

		@SerializedName("Maghrib")
		private String maghrib;

		@SerializedName("method")
		private List<String> method;

		@SerializedName("TengahMalam")
		private String tengahMalam;

		@SerializedName("Sunrise")
		private String sunrise;

		@SerializedName("DuapertigaMalam")
		private String duapertigaMalam;

		@SerializedName("SepertigaMalam")
		private String sepertigaMalam;

		public void setSunset(String sunset){
			this.sunset = sunset;
		}

		public String getSunset(){
			return sunset;
		}

		public void setAsr(String asr){
			this.asr = asr;
		}

		public String getAsr(){
			return asr;
		}

		public void setIsha(String isha){
			this.isha = isha;
		}

		public String getIsha(){
			return isha;
		}

		public void setFajr(String fajr){
			this.fajr = fajr;
		}

		public String getFajr(){
			return fajr;
		}

		public void setDhuhr(String dhuhr){
			this.dhuhr = dhuhr;
		}

		public String getDhuhr(){
			return dhuhr;
		}

		public void setMaghrib(String maghrib){
			this.maghrib = maghrib;
		}

		public String getMaghrib(){
			return maghrib;
		}

		public void setMethod(List<String> method){
			this.method = method;
		}

		public List<String> getMethod(){
			return method;
		}

		public void setTengahMalam(String tengahMalam){
			this.tengahMalam = tengahMalam;
		}

		public String getTengahMalam(){
			return tengahMalam;
		}

		public void setSunrise(String sunrise){
			this.sunrise = sunrise;
		}

		public String getSunrise(){
			return sunrise;
		}

		public void setDuapertigaMalam(String duapertigaMalam){
			this.duapertigaMalam = duapertigaMalam;
		}

		public String getDuapertigaMalam(){
			return duapertigaMalam;
		}

		public void setSepertigaMalam(String sepertigaMalam){
			this.sepertigaMalam = sepertigaMalam;
		}

		public String getSepertigaMalam(){
			return sepertigaMalam;
		}
	}
}