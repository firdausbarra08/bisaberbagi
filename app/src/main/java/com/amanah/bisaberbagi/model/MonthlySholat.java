package com.amanah.bisaberbagi.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MonthlySholat{

	@SerializedName("code")
	private int code;

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("status")
	private String status;

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public class DataItem{

		@SerializedName("date")
		private Date date;

		@SerializedName("meta")
		private Meta meta;

		@SerializedName("timings")
		private Timings timings;

		public void setDate(Date date){
			this.date = date;
		}

		public Date getDate(){
			return date;
		}

		public void setMeta(Meta meta){
			this.meta = meta;
		}

		public Meta getMeta(){
			return meta;
		}

		public void setTimings(Timings timings){
			this.timings = timings;
		}

		public Timings getTimings(){
			return timings;
		}
	}

	public class Meta{

		@SerializedName("method")
		private Method method;

		@SerializedName("offset")
		private Offset offset;

		@SerializedName("school")
		private String school;

		@SerializedName("timezone")
		private String timezone;

		@SerializedName("midnightMode")
		private String midnightMode;

		@SerializedName("latitude")
		private double latitude;

		@SerializedName("longitude")
		private double longitude;

		@SerializedName("latitudeAdjustmentMethod")
		private String latitudeAdjustmentMethod;

		public void setMethod(Method method){
			this.method = method;
		}

		public Method getMethod(){
			return method;
		}

		public void setOffset(Offset offset){
			this.offset = offset;
		}

		public Offset getOffset(){
			return offset;
		}

		public void setSchool(String school){
			this.school = school;
		}

		public String getSchool(){
			return school;
		}

		public void setTimezone(String timezone){
			this.timezone = timezone;
		}

		public String getTimezone(){
			return timezone;
		}

		public void setMidnightMode(String midnightMode){
			this.midnightMode = midnightMode;
		}

		public String getMidnightMode(){
			return midnightMode;
		}

		public void setLatitude(double latitude){
			this.latitude = latitude;
		}

		public double getLatitude(){
			return latitude;
		}

		public void setLongitude(double longitude){
			this.longitude = longitude;
		}

		public double getLongitude(){
			return longitude;
		}

		public void setLatitudeAdjustmentMethod(String latitudeAdjustmentMethod){
			this.latitudeAdjustmentMethod = latitudeAdjustmentMethod;
		}

		public String getLatitudeAdjustmentMethod(){
			return latitudeAdjustmentMethod;
		}

		public class Method{

			@SerializedName("name")
			private String name;

			@SerializedName("id")
			private int id;

			@SerializedName("params")
			private Params params;

			public void setName(String name){
				this.name = name;
			}

			public String getName(){
				return name;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}

			public void setParams(Params params){
				this.params = params;
			}

			public Params getParams(){
				return params;
			}
		}
	}

	public class Date{

		@SerializedName("readable")
		private String readable;

		@SerializedName("hijri")
		private Hijri hijri;

		@SerializedName("gregorian")
		private Gregorian gregorian;

		@SerializedName("timestamp")
		private String timestamp;

		public void setReadable(String readable){
			this.readable = readable;
		}

		public String getReadable(){
			return readable;
		}

		public void setHijri(Hijri hijri){
			this.hijri = hijri;
		}

		public Hijri getHijri(){
			return hijri;
		}

		public void setGregorian(Gregorian gregorian){
			this.gregorian = gregorian;
		}

		public Gregorian getGregorian(){
			return gregorian;
		}

		public void setTimestamp(String timestamp){
			this.timestamp = timestamp;
		}

		public String getTimestamp(){
			return timestamp;
		}

		public class Gregorian{

			@SerializedName("date")
			private String date;

			@SerializedName("month")
			private Month month;

			@SerializedName("year")
			private String year;

			@SerializedName("format")
			private String format;

			@SerializedName("weekday")
			private Weekday weekday;

			@SerializedName("designation")
			private Designation designation;

			@SerializedName("day")
			private String day;

			public void setDate(String date){
				this.date = date;
			}

			public String getDate(){
				return date;
			}

			public void setMonth(Month month){
				this.month = month;
			}

			public Month getMonth(){
				return month;
			}

			public void setYear(String year){
				this.year = year;
			}

			public String getYear(){
				return year;
			}

			public void setFormat(String format){
				this.format = format;
			}

			public String getFormat(){
				return format;
			}

			public void setWeekday(Weekday weekday){
				this.weekday = weekday;
			}

			public Weekday getWeekday(){
				return weekday;
			}

			public void setDesignation(Designation designation){
				this.designation = designation;
			}

			public Designation getDesignation(){
				return designation;
			}

			public void setDay(String day){
				this.day = day;
			}

			public String getDay(){
				return day;
			}
		}

		public class Hijri{

			@SerializedName("date")
			private String date;

			@SerializedName("month")
			private Month month;

			@SerializedName("holidays")
			private List<Object> holidays;

			@SerializedName("year")
			private String year;

			@SerializedName("format")
			private String format;

			@SerializedName("weekday")
			private Weekday weekday;

			@SerializedName("designation")
			private Designation designation;

			@SerializedName("day")
			private String day;

			public void setDate(String date){
				this.date = date;
			}

			public String getDate(){
				return date;
			}

			public void setMonth(Month month){
				this.month = month;
			}

			public Month getMonth(){
				return month;
			}

			public void setHolidays(List<Object> holidays){
				this.holidays = holidays;
			}

			public List<Object> getHolidays(){
				return holidays;
			}

			public void setYear(String year){
				this.year = year;
			}

			public String getYear(){
				return year;
			}

			public void setFormat(String format){
				this.format = format;
			}

			public String getFormat(){
				return format;
			}

			public void setWeekday(Weekday weekday){
				this.weekday = weekday;
			}

			public Weekday getWeekday(){
				return weekday;
			}

			public void setDesignation(Designation designation){
				this.designation = designation;
			}

			public Designation getDesignation(){
				return designation;
			}

			public void setDay(String day){
				this.day = day;
			}

			public String getDay(){
				return day;
			}
		}

		public class Month{

			@SerializedName("number")
			private int number;

			@SerializedName("en")
			private String en;

			@SerializedName("ar")
			private String ar;

			public void setNumber(int number){
				this.number = number;
			}

			public int getNumber(){
				return number;
			}

			public void setEn(String en){
				this.en = en;
			}

			public String getEn(){
				return en;
			}

			public void setAr(String ar){
				this.ar = ar;
			}

			public String getAr(){
				return ar;
			}


		}
	}

	public class Timings{

		@SerializedName("Sunset")
		private String sunset;

		@SerializedName("Asr")
		private String asr;

		@SerializedName("Isha")
		private String isha;

		@SerializedName("Fajr")
		private String fajr;

		@SerializedName("Dhuhr")
		private String dhuhr;

		@SerializedName("Maghrib")
		private String maghrib;

		@SerializedName("Sunrise")
		private String sunrise;

		@SerializedName("Midnight")
		private String midnight;

		@SerializedName("Imsak")
		private String imsak;

		public void setSunset(String sunset){
			this.sunset = sunset;
		}

		public String getSunset(){
			return sunset;
		}

		public void setAsr(String asr){
			this.asr = asr;
		}

		public String getAsr(){
			return asr;
		}

		public void setIsha(String isha){
			this.isha = isha;
		}

		public String getIsha(){
			return isha;
		}

		public void setFajr(String fajr){
			this.fajr = fajr;
		}

		public String getFajr(){
			return fajr;
		}

		public void setDhuhr(String dhuhr){
			this.dhuhr = dhuhr;
		}

		public String getDhuhr(){
			return dhuhr;
		}

		public void setMaghrib(String maghrib){
			this.maghrib = maghrib;
		}

		public String getMaghrib(){
			return maghrib;
		}

		public void setSunrise(String sunrise){
			this.sunrise = sunrise;
		}

		public String getSunrise(){
			return sunrise;
		}

		public void setMidnight(String midnight){
			this.midnight = midnight;
		}

		public String getMidnight(){
			return midnight;
		}

		public void setImsak(String imsak){
			this.imsak = imsak;
		}

		public String getImsak(){
			return imsak;
		}
	}

	public class Offset{

		@SerializedName("Sunset")
		private int sunset;

		@SerializedName("Asr")
		private int asr;

		@SerializedName("Isha")
		private int isha;

		@SerializedName("Fajr")
		private int fajr;

		@SerializedName("Dhuhr")
		private int dhuhr;

		@SerializedName("Maghrib")
		private int maghrib;

		@SerializedName("Sunrise")
		private int sunrise;

		@SerializedName("Midnight")
		private int midnight;

		@SerializedName("Imsak")
		private int imsak;

		public void setSunset(int sunset){
			this.sunset = sunset;
		}

		public int getSunset(){
			return sunset;
		}

		public void setAsr(int asr){
			this.asr = asr;
		}

		public int getAsr(){
			return asr;
		}

		public void setIsha(int isha){
			this.isha = isha;
		}

		public int getIsha(){
			return isha;
		}

		public void setFajr(int fajr){
			this.fajr = fajr;
		}

		public int getFajr(){
			return fajr;
		}

		public void setDhuhr(int dhuhr){
			this.dhuhr = dhuhr;
		}

		public int getDhuhr(){
			return dhuhr;
		}

		public void setMaghrib(int maghrib){
			this.maghrib = maghrib;
		}

		public int getMaghrib(){
			return maghrib;
		}

		public void setSunrise(int sunrise){
			this.sunrise = sunrise;
		}

		public int getSunrise(){
			return sunrise;
		}

		public void setMidnight(int midnight){
			this.midnight = midnight;
		}

		public int getMidnight(){
			return midnight;
		}

		public void setImsak(int imsak){
			this.imsak = imsak;
		}

		public int getImsak(){
			return imsak;
		}
	}

	public class Params{

		@SerializedName("Isha")
		private double isha;

		@SerializedName("Fajr")
		private double fajr;

		public void setIsha(double isha){
			this.isha = isha;
		}

		public double getIsha(){
			return isha;
		}

		public void setFajr(double fajr){
			this.fajr = fajr;
		}

		public double getFajr(){
			return fajr;
		}
	}

	public class Designation{

		@SerializedName("expanded")
		private String expanded;

		@SerializedName("abbreviated")
		private String abbreviated;

		public void setExpanded(String expanded){
			this.expanded = expanded;
		}

		public String getExpanded(){
			return expanded;
		}

		public void setAbbreviated(String abbreviated){
			this.abbreviated = abbreviated;
		}

		public String getAbbreviated(){
			return abbreviated;
		}
	}

	public class Weekday{

		@SerializedName("en")
		private String en;

		@SerializedName("ar")
		private String ar;

		public void setEn(String en){
			this.en = en;
		}

		public String getEn(){
			return en;
		}

		public void setAr(String ar){
			this.ar = ar;
		}

		public String getAr(){
			return ar;
		}
	}


}