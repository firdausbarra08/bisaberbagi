package com.amanah.bisaberbagi.ui.main;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.amanah.bisaberbagi.model.MonthlySholat;
import com.amanah.bisaberbagi.model.Qibla;
import com.amanah.bisaberbagi.other.helper.IntegerArrayHelper;
import com.amanah.bisaberbagi.other.helper.SharedPrefHelper;
import com.amanah.bisaberbagi.other.helper.Tools;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;

public class MainPresenter implements MainContract.MainPresenter {

    private Activity activity;
    private MainContract.MainView view;
    private SharedPreferences spAdjust;
    private Handler handlerCountDown;

    MainPresenter(Activity activity, MainContract.MainView view) {
        this.activity = activity;
        this.view = view;
        this.spAdjust = SharedPrefHelper.getSharedPreferencesAdjust(activity);
    }

    @Override
    public void getData() {
        Log.d("MainPresenter", "get data");

        if(checkSavedContent()){
            getSavedContent();
            Log.d("MainPresenter", "Saved content ready");

        }

        if(checkInternet() && getLocation()!= null){
            Log.d("MainPresenter", "internet is ready");
            getNewContent();
        }
    }

    @Override
    public void setHandlerCountdown() {
        this.handlerCountDown = new Handler();
    }

    @Override
    public Boolean checkSavedContent() {
        String locationLet = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LET_COORDINAT, null);
        String locationLong = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LANG_COORDINAT, null);
        String locationName = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LOCATION_NAME, null);
        String locationNameDetail = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LOCATION_NAME, null);
        float qiblaAzimuth = spAdjust.getFloat(SharedPrefHelper.Adjustment.FLOAT_LAST_QIBLA_AZIMUTH, 0);
        String jsonPrayer = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_JSON_MONTHLY_SHOLAT, null);

        return locationLet != null && locationLong != null && locationName != null && locationNameDetail != null && qiblaAzimuth != 0 && jsonPrayer != null;
    }

    @Override
    public void getSavedContent() {
        String locationName = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LOCATION_NAME, null);
        String locationNameDetail = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LOCATION_NAME, null);
        String jsonPrayer = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_JSON_MONTHLY_SHOLAT, null);

        MonthlySholat monthlySholat = new Gson().fromJson(jsonPrayer, MonthlySholat.class);


        if(monthlySholat.getData().get(0).getDate().getGregorian().getMonth().getNumber() == Integer.parseInt(Tools.getMonth())){

            String subuh = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getFajr();
            String dzuhur = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getDhuhr();
            String ashar = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getAsr();
            String maghrib = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getMaghrib();
            String isya = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getIsha();
            view.showTodayPrayerTimerContent(subuh, dzuhur, ashar, maghrib, isya);

            getNextPrayerTime(monthlySholat);
        }

        getHijriCalendar();
        view.showLocationContent(locationName, locationNameDetail);
    }

    @Override
    public Boolean checkInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert connectivityManager != null;
        return Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)).getState() == NetworkInfo.State.CONNECTED ||
                Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)).getState() == NetworkInfo.State.CONNECTED;
    }

    @Override
    public Location getLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        MainActivity.REQUEST_LOCATION_PREMISION);
                return null;
            }
        }

        LocationManager locationManager = (LocationManager) activity.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            return locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        return null;
    }

    @Override
    public void getPlaceWithNameAndDetail() {
        SmartLocation.with(activity).geocoding().reverse(getLocation(), new OnReverseGeocodingListener() {
            @Override
            public void onAddressResolved(Location location, List<Address> list) {
                SharedPreferences.Editor editor = spAdjust.edit();
                editor.putString(SharedPrefHelper.Adjustment.STRING_LAST_LOCATION_NAME, list.get(0).getCountryName());
                editor.putString(SharedPrefHelper.Adjustment.STRING_LAST_LOCATION_NAME_DETAIL, list.get(0).getLocality());
                editor.putString(SharedPrefHelper.Adjustment.STRING_LAST_LET_COORDINAT, String.valueOf(getLocation().getLatitude()));
                editor.putString(SharedPrefHelper.Adjustment.STRING_LAST_LANG_COORDINAT, String.valueOf(getLocation().getLongitude()));
                editor.apply();
                view.showLocationContent(list.get(0).getAdminArea(), list.get(0).getLocality());
            }
        });
    }

    @Override
    public void getNewContent() {
        getPrayerTime();
        getNewQibla();
        getPlaceWithNameAndDetail();
        getHijriCalendar();
        Log.d("MainPresenter", "start new data");
    }

    @Override
    public float getSavedQibla() {
        return spAdjust.getFloat(SharedPrefHelper.Adjustment.FLOAT_LAST_QIBLA_AZIMUTH, 0) ;
    }

    @Override
    public void getNewQibla() {
        Tools.goTools().setData(String.valueOf(getLocation().getLatitude()), String.valueOf(getLocation().getLongitude()), null).getQibla(new Tools.ToolsInterface.OnGetQiblaListener() {
            @Override
            public void getQiblaDirection(Qibla qibla) {
                SharedPreferences.Editor editor = spAdjust.edit();
                editor.putFloat(SharedPrefHelper.Adjustment.FLOAT_LAST_QIBLA_AZIMUTH, (float) qibla.getData().getDerajat());
                editor.apply();

                Log.d("MainPresenter", "get new qibla!" + " :" + qibla.getData().getDerajat());

            }

            @Override
            public void onError(String msg) {

            }
        });

    }

    @Override
    public void getHijriCalendar() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("", Locale.ENGLISH);
        simpleDateFormat.applyPattern("d MMMM, y");

        //Hijriyah
        UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
        String hijriDay = String.valueOf(ummalquraCalendar.get(Calendar.DAY_OF_MONTH));
        String hijriMonth = ummalquraCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG,  Locale.ENGLISH);
        String hijriYear = String.valueOf(ummalquraCalendar.get(Calendar.YEAR));
        String hijriDate = hijriDay + " " + hijriMonth + " " + hijriYear;

        //Georgian
        Date date = new Date();
        String georgyDate = simpleDateFormat.format(date);

        view.showDateContent(hijriDate, georgyDate);

    }

    @Override
    public void getNextPrayerTime(MonthlySholat monthlySholat) {


        int[] prayerTime = new int[5];

        String subuh = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getFajr();
        String dzuhur = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getDhuhr();
        String ashar = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getAsr();
        String maghrib = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getMaghrib();
        String isya = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getIsha();

        prayerTime[0] = Integer.parseInt(subuh.substring(0,2));
        prayerTime[1] = Integer.parseInt(dzuhur.substring(0,2));
        prayerTime[2] = Integer.parseInt(ashar.substring(0,2));
        prayerTime[3] = Integer.parseInt(maghrib.substring(0,2));
        prayerTime[4] = Integer.parseInt(isya.substring(0,2));

        Log.d("MainPresenter", String.valueOf(prayerTime[0]));
        Log.d("MainPresenter", String.valueOf(prayerTime[1]));
        Log.d("MainPresenter", String.valueOf(prayerTime[2]));
        Log.d("MainPresenter", String.valueOf(prayerTime[3]));
        Log.d("MainPresenter", String.valueOf(prayerTime[4]));

        int nowHour = Integer.parseInt(Tools.getHours());

        int index = IntegerArrayHelper.findClosest(prayerTime, nowHour);

        switch (IntegerArrayHelper.findIndex(prayerTime, index)) {
            case 0 :
                view.showNextPrayerTimeContent("Subuh", monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getFajr());
                getNextPrayerCountdown(monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getFajr());
                break;
            case 1 :
                view.showNextPrayerTimeContent("Dzuhur", monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getDhuhr());
                getNextPrayerCountdown(monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getDhuhr());
                break;
            case  2 :
                view.showNextPrayerTimeContent("Ashar", monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getAsr());
                getNextPrayerCountdown(monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getAsr());
                break;
            case  3:
                view.showNextPrayerTimeContent("Maghrib", monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getMaghrib());
                getNextPrayerCountdown(monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getMaghrib());
                break;
            case 4 :
                view.showNextPrayerTimeContent("Isya", monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getIsha());
                getNextPrayerCountdown(monthlySholat.getData().get(Integer.parseInt(Tools.getDay())-1).getTimings().getIsha());
                break;

                default:
                    Log.d("MainPresenter", "error index");
                    onFailAndError();
                    break;
        }
    }

    @Override
    public void getNextPrayerCountdown(String time) {
        setHandlerCountdown();

        final String sholatTime = time.substring(0,5) + ":00";

        //result
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    Date current_date = new Date();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
                    Date sholatDate = dateFormat.parse(sholatTime);

                    if (handlerCountDown != null) {
                        handlerCountDown.postDelayed(this, 1000);
                    }

                    if (current_date.getTime() > Objects.requireNonNull(sholatDate).getTime()) {

                        //result
                        long margin = Objects.requireNonNull(current_date.getTime()) - Objects.requireNonNull(sholatDate).getTime();
                        long hours = (margin / (60 * 60 * 1000) % 24 - 23) * -1;
                        long minutes = (margin / (60 * 1000) % 60 - 60) * -1;
                        long second = (margin / (1000) % 60 - 60) * -1;

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY, (int) hours);
                        calendar.set(Calendar.MINUTE, (int) minutes);
                        calendar.set(Calendar.SECOND, (int) second);

                        String ok = "-" + dateFormat.format(calendar.getTime().getTime());


                        if(hours>20){
                            stopHandlerCountDown();
                            view.showNextPrayerCountDown("-");
                        }else {
                            view.showNextPrayerCountDown(ok);
                        }

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        };

        if(handlerCountDown!=null){
            handlerCountDown.postDelayed(runnable, 0);
        }

    }

    @Override
    public void stopHandlerCountDown() {
        handlerCountDown = null;
    }

    @Override
    public void getPrayerTime() {
        Tools.goTools().setData(String.valueOf(getLocation().getLatitude()), String.valueOf(getLocation().getLongitude()), null).getMonthlySholat(new Tools.ToolsInterface.OnGetMonthlySholatListener() {
            @Override
            public void getJsonMonthlySholat(JSONObject jsonObject) {
                SharedPreferences.Editor editor = spAdjust.edit();
                editor.putString(SharedPrefHelper.Adjustment.STRING_JSON_MONTHLY_SHOLAT,jsonObject.toString());
                editor.apply();

                MonthlySholat monthlySholat = new Gson().fromJson(jsonObject.toString(), MonthlySholat.class);
                if(monthlySholat.getData().get(0).getDate().getGregorian().getMonth().getNumber() == Integer.parseInt(Tools.getMonth())){
                    String subuh = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getFajr();
                    String dzuhur = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getDhuhr();
                    String ashar = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getAsr();
                    String maghrib = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getMaghrib();
                    String isya = monthlySholat.getData().get(Integer.parseInt(Tools.getDay()) - 1).getTimings().getIsha();
                    view.showTodayPrayerTimerContent(subuh, dzuhur, ashar, maghrib, isya);
                    getNextPrayerTime(monthlySholat);

                    Log.d("MainPresenter", "get MountlySholat succes");

                }else{
                    onFailAndError();
                }

                Log.d("MainPresenter", "get MountlySholat something happen");

            }

            @Override
            public void onError(String msg) {

            }
        });
    }

    @Override
    public void onFailAndError() {

    }
}
