package com.amanah.bisaberbagi.ui.qibla;

import android.content.Context;
import android.location.Location;

public interface QiblaContract {
    interface QiblaView {
        void getExtraAndRun();
        void showLocationAndQiblaAzimuth(String qiblaDegrees, String locationName);
        void onGetCompassAzimuth(float azimuth);
        void rotateNortPoin(float phoneAzimuth, float qiblaAzimuth);
        void rotateQiblaPoin(float phoneAzimuth, float qiblaAzimuth);
        void somethingError(String msg);
    }

    interface QiblaPresenter {
        void initCompass();
        void startCompass();
        void stopCompass();
        void getAzimuthFromCompass();
    }
}
