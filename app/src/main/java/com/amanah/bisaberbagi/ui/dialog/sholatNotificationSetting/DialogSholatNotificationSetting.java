package com.amanah.bisaberbagi.ui.dialog.sholatNotificationSetting;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.amanah.bisaberbagi.R;
import com.amanah.bisaberbagi.other.helper.SharedPrefHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogSholatNotificationSetting extends DialogFragment implements SeekBar.OnSeekBarChangeListener {
    private SeekBar sbSubuh, sbDzuhur, sbAshar, sbMaghrib, sbIsya;
    private TextView notifInfoSubuh, notifInfoDzuhur, notifInfoAshar, notifInfoMaghrib, notifInfoIsya;

    SharedPreferences spNotifAdjustment;


    public DialogSholatNotificationSetting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_sholat_notification_setting, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //initialization
        sbSubuh = view.findViewById(R.id.skb_adzan_subuh);
        sbDzuhur = view.findViewById(R.id.skb_adzan_dzuhur);
        sbAshar = view.findViewById(R.id.skb_adzan_ashar);
        sbMaghrib = view.findViewById(R.id.skb_adzan_maghrib);
        sbIsya = view.findViewById(R.id.skb_adzan_isya);
        notifInfoSubuh = view.findViewById(R.id.info_notif_subuh);
        notifInfoDzuhur = view.findViewById(R.id.info_notif_dzuhur);
        notifInfoAshar = view.findViewById(R.id.info_notif_ashar);
        notifInfoMaghrib = view.findViewById(R.id.info_notif_maghrib);
        notifInfoIsya = view.findViewById(R.id.info_notif_isya);

        if(getActivity()!=null){
            spNotifAdjustment = SharedPrefHelper.getSharedPreferencesNotifAdjust(getActivity());
            if (getDialog() != null && getDialog().getWindow() != null) {
                getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                setToSetting();
            }else{
            }
        }

        sbSubuh.setOnSeekBarChangeListener(this);

    }

    void setToSetting(){
        int progressSubuh = spNotifAdjustment.getInt(SharedPrefHelper.NotifAdjust.SUBUH, 0);
        int progressDzuhur = spNotifAdjustment.getInt(SharedPrefHelper.NotifAdjust.DZUHUR, 0);
        int progressAshar = spNotifAdjustment.getInt(SharedPrefHelper.NotifAdjust.ASHAR, 0);
        int progressMaghrib = spNotifAdjustment.getInt(SharedPrefHelper.NotifAdjust.MAGHRIB, 0);
        int progressIsya = spNotifAdjustment.getInt(SharedPrefHelper.NotifAdjust.ISYA, 0);

        sbSubuh.setProgress(progressSubuh);
        sbDzuhur.setProgress(progressDzuhur);
        sbAshar.setProgress(progressAshar);
        sbMaghrib.setProgress(progressMaghrib);
        sbIsya.setProgress(progressIsya);

        String infoSubuh = getActivity().getResources().getString(R.string.adzan_alarm) + " " + progressToMinute(progressSubuh) + " " + getString(R.string.minute);
        String infoDzuhur = getActivity().getResources().getString(R.string.adzan_alarm) + " " + progressToMinute(progressDzuhur) + " " + getString(R.string.minute);
        String infoAshar = getActivity().getResources().getString(R.string.adzan_alarm) + " " + progressToMinute(progressAshar) + " " + getString(R.string.minute);
        String infoMaghrib = getActivity().getResources().getString(R.string.adzan_alarm) + " " + progressToMinute(progressMaghrib) + " " + getString(R.string.minute);
        String infoIsya = getActivity().getResources().getString(R.string.adzan_alarm) + " " + progressToMinute(progressIsya) + " " + getString(R.string.minute);

        notifInfoSubuh.setText(infoSubuh);
        notifInfoDzuhur.setText(infoDzuhur);
        notifInfoAshar.setText(infoAshar);
        notifInfoMaghrib.setText(infoMaghrib);
        notifInfoIsya.setText(infoIsya);

    }

    int progressToMinute(int progress){
        switch (progress) {
            case 0 :
                return 5;
            case 1 :
                return 4;
            case 2 :
                return 3;
            case 3 :
                return 2;
            case 4 :
                return 1;
                default :
                    return 0;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
        SharedPreferences.Editor editor = spNotifAdjustment.edit();

        switch (seekBar.getId()){
            case R.id.skb_adzan_subuh :
                editor.putInt(SharedPrefHelper.NotifAdjust.SUBUH, progress);
                editor.apply();
                setToSetting();
                break;
            case R.id.skb_adzan_dzuhur :
                editor.putInt(SharedPrefHelper.NotifAdjust.DZUHUR, progress);
                editor.apply();
                setToSetting();
                break;
            case R.id.skb_adzan_ashar :
                editor.putInt(SharedPrefHelper.NotifAdjust.ASHAR, progress);
                editor.apply();
                setToSetting();
                break;
            case R.id.skb_adzan_maghrib :
                editor.putInt(SharedPrefHelper.NotifAdjust.MAGHRIB, progress);
                editor.apply();
                setToSetting();
                break;
            case R.id.skb_adzan_isya :
                editor.putInt(SharedPrefHelper.NotifAdjust.ISYA, progress);
                editor.apply();
                setToSetting();
                break;
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
