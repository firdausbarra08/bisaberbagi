package com.amanah.bisaberbagi.ui.qibla;

import android.content.Context;

import com.amanah.bisaberbagi.other.helper.CompasHelper;

public class QiblaPresenter implements QiblaContract.QiblaPresenter {
    private Context context;
    private QiblaContract.QiblaView view;

    private CompasHelper compasHelper;

    QiblaPresenter(Context context, QiblaContract.QiblaView view) {
        this.context = context;
        this.view = view;
    }

    @Override
    public void initCompass() {
        compasHelper = new CompasHelper(context);
    }

    @Override
    public void startCompass() {
        compasHelper.startSensor();

    }

    @Override
    public void stopCompass() {
        compasHelper.stopSensor();

    }

    @Override
    public void getAzimuthFromCompass() {
        compasHelper.setListener(new CompasHelper.CompassListener() {
            @Override
            public void onNewAzimuth(float azimuth) {
                view.onGetCompassAzimuth(azimuth);
            }
        });
    }

}
