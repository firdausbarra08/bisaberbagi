package com.amanah.bisaberbagi.ui.dialog.sholatInfo;


import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.amanah.bisaberbagi.R;
import com.amanah.bisaberbagi.other.helper.SharedPrefHelper;
import com.amanah.bisaberbagi.other.helper.Tools;
import com.amanah.bisaberbagi.model.JadwalSholat;
import com.amanah.bisaberbagi.model.MonthlySholat;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class DialogSholatInfo extends DialogFragment {
    TextView dateHijri, dateGeo;

    ImageButton btnLeftArrowDialogSholat, btnRightArrowDialogSholat;
    ImageButton btnSoundSholatDialogSubuh;

    TextView txtDialogTimeSubuh, txtDialogTimeSyuruq, txtDialogTimeDzuhur, txtdialogTimeAshar, txtDialogTimeMaghrib, txtDialogTimeIsya;

    ProgressBar progressBar;
    ConstraintLayout constraintLayout;

    ImageButton btnSoundSholatDialogSyuruq,
            btnSoundSholatDialogDzuhur,
            btnSoundSholatDialogAshar,
            btnSoundSholatDialogMaghrib,
            btnSoundSholatDialogIsya;
    Button btnClose;

    private int dateIndex = 0;
    private SharedPreferences spAdjust;

    public DialogSholatInfo() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.dialog_sholat_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //initialization
        dateHijri = view.findViewById(R.id.date_hijri);
        dateGeo = view.findViewById(R.id.date_geo);

        btnRightArrowDialogSholat = view.findViewById(R.id.btn_right_arrow_dialog_sholat);
        btnLeftArrowDialogSholat = view.findViewById(R.id.btn_left_arrow_dialog_sholat);

        txtDialogTimeSubuh = view.findViewById(R.id.txt_dialog_time_subuh);
        txtDialogTimeSyuruq = view.findViewById(R.id.txt_dialog_time_syuruq);
        txtDialogTimeDzuhur = view.findViewById(R.id.txt_dialog_time_dzuhur);
        txtdialogTimeAshar = view.findViewById(R.id.txtdialog_time_ashar);
        txtDialogTimeMaghrib = view.findViewById(R.id.txt_dialog_time_maghrib);
        txtDialogTimeIsya = view.findViewById(R.id.txt_dialog_time_isya);

        //loading effect
        constraintLayout = view.findViewById(R.id.constScheduleShola);
        progressBar = view.findViewById(R.id.progress_dialog_sholat);

        //close
        btnClose = view.findViewById(R.id.btn_close);

        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        if(getActivity()!=null){
            spAdjust = SharedPrefHelper.getSharedPreferencesAdjust(getActivity());
            loadADay(dateIndex);
        }

        btnLeftArrowDialogSholat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateIndex = dateIndex - 1;
                loadADay(dateIndex);
            }
        });

        btnRightArrowDialogSholat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dateIndex = dateIndex + 1;
                loadADay(dateIndex);
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void loadADay(int dayAdjustment){
        String jsonSholat = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_JSON_MONTHLY_SHOLAT, null);
        if(jsonSholat != null){

            MonthlySholat currentMonthSholat = new Gson().fromJson(jsonSholat, MonthlySholat.class);
            String monthlySholatDate = currentMonthSholat.getData().get(Integer.parseInt(getDate(dayAdjustment)) - 1).getDate().getGregorian().getDay();
            String monthlySholatMonth = String.valueOf(currentMonthSholat.getData().get(Integer.parseInt(getDate(dayAdjustment))- 1).getDate().getGregorian().getMonth().getNumber());

            if(getDate(dayAdjustment).equals(monthlySholatDate)&&getMonth(dayAdjustment).equals(monthlySholatMonth)){
                loadBySavedPrayerSchedule(currentMonthSholat.getData().get(Integer.parseInt(getDate(dayAdjustment))-1).getTimings());
            }else {
                loadNewDailyPrayerSchedule(dayAdjustment);
            }

        }else{
            Log.d("DialogSholat", "null json object");
            loadNewDailyPrayerSchedule(dayAdjustment);
        }

        showHijriAndGeorgyDate(dayAdjustment);
    }

    private void loadBySavedPrayerSchedule(MonthlySholat.Timings sholatTiming){
        Log.d("DialogSholat", "load saved");

        String subuh = sholatTiming.getFajr();
        String syuruq = sholatTiming.getSunrise();
        String dzuhur = sholatTiming.getDhuhr();
        String ashar = sholatTiming.getAsr();
        String maghrib = sholatTiming.getMaghrib();
        String isya = sholatTiming.getIsha();

        txtDialogTimeSubuh.setText(subuh);
        txtDialogTimeSyuruq.setText(syuruq);
        txtDialogTimeDzuhur.setText(dzuhur);
        txtdialogTimeAshar.setText(ashar);
        txtDialogTimeMaghrib.setText(maghrib);
        txtDialogTimeIsya.setText(isya);
    }

    private void loadNewDailyPrayerSchedule(int adjustment){
        Log.d("DialogSholat", "load new");


        String date = Tools.getDate(adjustment);
        String lat = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LET_COORDINAT, null);
        String lang = spAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LANG_COORDINAT, null);

        showLoading();
        Tools.goTools().getDailySholatBySelf(lat, lang, date, new Tools.ToolsInterface.OnGetDailySholatListener() {
            @Override
            public void getDailySholat(JadwalSholat jadwalSholat) {
                Log.d("DialogSholat", "on get data");

                String subuh = jadwalSholat.getData().getFajr();
                String syuruq = jadwalSholat.getData().getSunrise();
                String dzuhur = jadwalSholat.getData().getDhuhr();
                String ashar = jadwalSholat.getData().getAsr();
                String maghrib = jadwalSholat.getData().getMaghrib();
                String isya = jadwalSholat.getData().getIsha();

                hideLoading();
                txtDialogTimeSubuh.setText(subuh);
                txtDialogTimeSyuruq.setText(syuruq);
                txtDialogTimeDzuhur.setText(dzuhur);
                txtdialogTimeAshar.setText(ashar);
                txtDialogTimeMaghrib.setText(maghrib);
                txtDialogTimeIsya.setText(isya);
            }

            @Override
            public void onError(String msg) {
                hideLoading();
                showError();
            }
        });


    }

    void showHijriAndGeorgyDate(int adjustment){
        UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
        ummalquraCalendar.add(Calendar.DATE, adjustment);

        String hijriDay = String.valueOf(ummalquraCalendar.get(Calendar.DAY_OF_MONTH));
        String hijriMonth = ummalquraCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG,  Locale.ENGLISH);
        String hijriYear = String.valueOf(ummalquraCalendar.get(Calendar.YEAR));
        String hijriDate = hijriDay + " " + hijriMonth + " " + hijriYear;

        //Georgian
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, adjustment);
        calendar.getTime();
        String georgyDate = new SimpleDateFormat("dd MMMM y", Locale.ENGLISH).format(calendar.getTime());

        dateHijri.setText(hijriDate);
        dateGeo.setText(georgyDate);
    }

    private String getDate(int adjustment){
        return Tools.getDayWithAdjust(adjustment);
    }

    private String getMonth(int adjustment){
        return Tools.getMonthWithDayAdjustment(adjustment);
    }

    private String getYears(){
        return Tools.getYear();
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        constraintLayout.setVisibility(View.GONE);
    }

    private void hideLoading(){
        progressBar.setVisibility(View.GONE);
        constraintLayout.setVisibility(View.VISIBLE);
    }

    private void showError(){}





}
