package com.amanah.bisaberbagi.ui.qibla;

import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.amanah.bisaberbagi.R;

public class QiblaActivity extends AppCompatActivity implements QiblaContract.QiblaView {
    //extra data keyword
    public static String LAT = "let";
    public static String LANG = "lang";
    public static String LOCATION_NAME = "location_name";
    public static String QIBLA_AZIMUTH = "qibla_azimuth";

    private float currentAzimuth = 0;
    private float qiblaAzimuth = 0;

    private QiblaContract.QiblaPresenter presenter;
    private TextView txtQiblaDegrees, txtLocationName;
    private ImageView imgNeedlePlace, imgNeedle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qibla);

        //initialization
        txtLocationName = findViewById(R.id.txt_location_qibla);
        txtQiblaDegrees = findViewById(R.id.txt_qibla_azimuth);
        imgNeedle = findViewById(R.id.img_needle);
        imgNeedlePlace = findViewById(R.id.img_needle_place);

        //presenter init
        presenter = new QiblaPresenter(this, this);
        presenter.initCompass();

        //get qibla
        getExtraAndRun();

    }

    @Override
    public void getExtraAndRun() {
        float currentLat = getIntent().getFloatExtra(LAT, 0);
        float currentLang = getIntent().getFloatExtra(LANG, 0);
        float currentQiblaAzimuth = getIntent().getFloatExtra(QIBLA_AZIMUTH, 0);
        String currentLocation = getIntent().getStringExtra(LOCATION_NAME);

        Log.d("QiblaActivity", currentLocation + " " + currentLat + " " + currentLang + " " + currentQiblaAzimuth);

        //save the qibla azimuth!
        this.qiblaAzimuth = currentQiblaAzimuth;

        if(currentLat != 0 && currentLang != 0 && currentQiblaAzimuth != 0 && currentLocation != null){
            showLocationAndQiblaAzimuth(String.valueOf(qiblaAzimuth), currentLocation);
            presenter.getAzimuthFromCompass();
        }else{
            String errrorMessage = getString(R.string.location_error);
            somethingError(errrorMessage);
        }
        //if success onGetQiblaDirection() method will be run
        //else somethingError will be run
    }

    @Override
    public void showLocationAndQiblaAzimuth(String qiblaDegrees, String locationName) {
        txtLocationName.setText(locationName);
        txtQiblaDegrees.setText(qiblaDegrees);
    }

    @Override
    public void onGetCompassAzimuth(float azimuth) {
        rotateNortPoin(azimuth, qiblaAzimuth);
        rotateQiblaPoin(azimuth, qiblaAzimuth);
        this.currentAzimuth = azimuth;

    }

    @Override
    public void rotateNortPoin(float phoneAzimuth, float qiblaAzimuth) {
        Animation animation = new RotateAnimation(-currentAzimuth, -phoneAzimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);

        animation.setDuration(500);
        animation.setRepeatCount(0);
        animation.setFillAfter(true);

        imgNeedlePlace.startAnimation(animation);
    }

    @Override
    public void rotateQiblaPoin(float phoneAzimuth, float qiblaAzimuth) {
        Animation animation = new RotateAnimation(-currentAzimuth + qiblaAzimuth, -phoneAzimuth,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);

        animation.setDuration(500);
        animation.setRepeatCount(0);
        animation.setFillAfter(true);

        imgNeedle.startAnimation(animation);
    }

    @Override
    public void somethingError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.startCompass();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.stopCompass();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.stopCompass();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.startCompass();
    }
}
