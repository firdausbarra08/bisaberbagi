package com.amanah.bisaberbagi.ui.main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.amanah.bisaberbagi.R;
import com.amanah.bisaberbagi.other.helper.SharedPrefHelper;
import com.amanah.bisaberbagi.ui.dialog.sholatInfo.DialogSholatInfo;
import com.amanah.bisaberbagi.ui.dialog.sholatNotificationSetting.DialogSholatNotificationSetting;
import com.amanah.bisaberbagi.ui.qibla.QiblaActivity;

public class MainActivity extends AppCompatActivity implements MainContract.MainView, View.OnClickListener {
    MainContract.MainPresenter presenter;
    public static final int REQUEST_LOCATION_PREMISION = 4;

    private ImageButton imgQibla, btn_sound_sholat_dialog;
    private TextView txt_place, txt_place_detail, txt_date_hijr, txt_date_georgy;
    private TextView txt_time_subuh, txt_time_dzuhur, txt_time_ashar, txt_time_maghrib, txt_time_isya;
    private TextView txt_next_sholat, txt_next_sholat_time, txt_countdown_adzan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imgQibla = findViewById(R.id.img_btn_compass);
        imgQibla.setOnClickListener(this);

        //for date and place
        txt_place = findViewById(R.id.txt_place);
        txt_place_detail = findViewById(R.id.txt_detail_place);
        txt_date_hijr = findViewById(R.id.txt_month_hijri);
        txt_date_georgy = findViewById(R.id.txt_month_geo);

        //for sholat
        txt_time_subuh = findViewById(R.id.txt_time_sholat_shubuh);
        txt_time_dzuhur = findViewById(R.id.txt_time_sholat_dzuhur);
        txt_time_ashar = findViewById(R.id.txt_time_sholat_ashar);
        txt_time_maghrib = findViewById(R.id.txt_time_sholat_maghrib);
        txt_time_isya = findViewById(R.id.txt_time_sholat_isya);

        //for countdown adzan
        txt_next_sholat = findViewById(R.id.txt_next_prayer);
        txt_next_sholat_time = findViewById(R.id.txt_next_prayer_time);
        txt_countdown_adzan = findViewById(R.id.txt_countdown_adzan);
        txt_next_sholat_time.setOnClickListener(this);

        //for dialog notification
        btn_sound_sholat_dialog = findViewById(R.id.btn_sound_sholat_dialog);
        btn_sound_sholat_dialog.setOnClickListener(this);

        //initialization
        presenter = new MainPresenter(this, this);
        presenter.getData();


    }

    @Override
    public void goToQibla() {
        SharedPreferences sharedPreferencesAdjust = SharedPrefHelper.getSharedPreferencesAdjust(this);

        Intent intent = new Intent(MainActivity.this, QiblaActivity.class);
        intent.putExtra(QiblaActivity.QIBLA_AZIMUTH, sharedPreferencesAdjust.getFloat(SharedPrefHelper.Adjustment.FLOAT_LAST_QIBLA_AZIMUTH, 0));
        intent.putExtra(QiblaActivity.LOCATION_NAME, sharedPreferencesAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LOCATION_NAME_DETAIL, null));
        intent.putExtra(QiblaActivity.LAT, Float.parseFloat(sharedPreferencesAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LET_COORDINAT, "")));
        intent.putExtra(QiblaActivity.LANG, Float.parseFloat(sharedPreferencesAdjust.getString(SharedPrefHelper.Adjustment.STRING_LAST_LANG_COORDINAT, "")));

        startActivity(intent);
    }

    @Override
    public void showTodayPrayerTimerContent(String subuh, String dzuhur, String ashar, String maghrib, String isya) {
        txt_time_subuh.setText(subuh);
        txt_time_dzuhur.setText(dzuhur);
        txt_time_ashar.setText(ashar);
        txt_time_maghrib.setText(maghrib);
        txt_time_isya.setText(isya);
    }

    @Override
    public void showLocationContent(String placeName, String placeDetail) {
        txt_place.setText(placeName);
        txt_place_detail.setText(placeDetail);
    }

    @Override
    public void showDateContent(String hijriDate, String georgianDate) {
        if(hijriDate != null && georgianDate != null){
            txt_date_hijr.setText(hijriDate);
            txt_date_georgy.setText(georgianDate);
        }
    }

    @Override
    public void showNextPrayerTimeContent(String sholat, String time) {
        txt_next_sholat.setText(sholat);
        txt_next_sholat_time.setText(time.substring(0, 5));
    }

    @Override
    public void showNextPrayerCountDown(String time) {
        txt_countdown_adzan.setText(time);
    }

    @Override
    public void showPrayerScheduleDialog() {
        DialogFragment dialogFragment = new DialogSholatInfo();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_btn_compass :
                goToQibla();
                break;

            case R.id.txt_next_prayer_time :
                showPrayerScheduleDialog();
                break;

            case R.id.btn_sound_sholat_dialog:
                showNotificationDialog();
                break;
        }
    }

    private void showNotificationDialog() {
        DialogFragment dialogFragment = new DialogSholatNotificationSetting();
        dialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == MainActivity.REQUEST_LOCATION_PREMISION){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.getData();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.stopHandlerCountDown();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.stopHandlerCountDown();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getData();
    }
}

