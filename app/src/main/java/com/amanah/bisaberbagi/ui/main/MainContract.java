package com.amanah.bisaberbagi.ui.main;
import android.app.Activity;
import android.content.Context;
import android.location.Location;

import com.amanah.bisaberbagi.model.MonthlySholat;

import java.util.Date;

public interface MainContract {
    interface MainView{
        void goToQibla();
        void showTodayPrayerTimerContent(String subuh, String dzuhur, String ashar, String maghrib, String isya);
        void showLocationContent(String placeName, String placeDetail);
        void showDateContent(String hijriDate, String georgianDate);
        void showNextPrayerTimeContent(String sholat, String time);
        void showNextPrayerCountDown(String time);
        void showPrayerScheduleDialog();

    }

    interface MainPresenter{
        void getData();
        Boolean checkSavedContent();
        void getSavedContent();
        Boolean checkInternet();
        Location getLocation();
        void getPlaceWithNameAndDetail();
        void getNewContent();
        float getSavedQibla();
        void getNewQibla();
        void getHijriCalendar();
        void getNextPrayerTime(MonthlySholat monthlySholat);
        void getNextPrayerCountdown(String time);
        void stopHandlerCountDown();
        void setHandlerCountdown();
        void getPrayerTime();
        void onFailAndError();
    }
}
